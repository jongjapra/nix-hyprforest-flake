# RESOURCE: https://github.com/ryan4yin/nix-config/blob/d20760cd619760e69e2a36438793df46113a39a7/lib/default.nix
{lib, ...}: path:
builtins.map
(f: (path + "/${f}"))
(builtins.attrNames
  (lib.attrsets.filterAttrs
    (
      path: _type:
        (_type == "directory") # Include directories
        || (
          (path != "default.nix") # Ignore default.nix
          && (lib.strings.hasSuffix ".nix" path) # Include .nix files
        )
    )
    (builtins.readDir path)))
