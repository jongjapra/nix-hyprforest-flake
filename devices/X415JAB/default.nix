{
  inputs,
  lib,
  src,
  ...
}: let
  vars = import (src + /variables.nix);
  scanPaths = import (src + /flake-libs/scan-paths.nix) {inherit lib;};
in {
  system = vars.system;
  specialArgs = {inherit inputs scanPaths src vars;};
  modules =
    [
      inputs.disko.nixosModules.disko
      ./disko-config.nix
      ./home-manager.nix
    ]
    ++ scanPaths (src + /nixos-modules);
}
