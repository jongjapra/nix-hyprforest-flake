{
  inputs,
  scanPaths,
  src,
  vars,
  ...
}: {
  imports = with inputs; [home-manager.nixosModules.home-manager];

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    extraSpecialArgs = {inherit inputs scanPaths src vars;};
    users = with vars; {
      ${username} = {
        programs.home-manager.enable = true;
        home = {inherit username stateVersion;};
        home.homeDirectory = "/home/" + username;
        imports = scanPaths (src + /home-modules);
      };
    };
  };
}
