{
  # System Variables
  device = "X415JAB";
  system = "x86_64-linux";
  stateVersion = "24.05";
  hostname = "nixos";
  keyboard = "colemak_dh";
  locale = "en_US.UTF-8";
  timezone = "Asia/Jakarta";

  # User Variables
  fullname = "Randi Budi";
  username = "jongjapra";
  email = "jongjapra@skiff.com";
  password = "$6$luA5XJaUzGvdfeYI$nvErFj1V0YWfbGxFNJvCUR6mUAaf2RbKlmxHFfQbFqwbsavtesBldY4Iev9rSMf3jNfH1iAeAiLYxey15yHo.0";
}
