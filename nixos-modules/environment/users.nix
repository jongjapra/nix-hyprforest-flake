{vars, ...}: {
  users = with vars; {
    mutableUsers = false;
    users.root.initialHashedPassword = password;
    users.${username} = {
      isNormalUser = true;
      description = fullname;
      initialHashedPassword = password;
      extraGroups = ["wheel"];
    };
  };
}
