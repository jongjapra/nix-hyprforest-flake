{pkgs, ...}: {
  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    # Archives
    p7zip
    unzip
    xz
    zip

    # Downloads
    curl
    wget

    # Hyprland Environment
    brightnessctl
    librewolf-wayland
    pulsemixer
    wezterm
    wl-clipboard

    # CLI
    git
    htop
    neofetch
    neovim
    nh
  ];
}
