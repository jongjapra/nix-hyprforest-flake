{
  nix.settings = {
    auto-optimise-store = true;
    experimental-features = ["nix-command" "flakes"];
    substituters = ["https://cache.komunix.org"];
    trusted-users = ["root" "@wheel"];
    warn-dirty = false;
  };
}
