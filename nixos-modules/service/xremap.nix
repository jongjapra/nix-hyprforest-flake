{inputs, ...}: {
  imports = with inputs; [xremap.nixosModules.default];

  services.xremap.withWlroots = true;
  services.xremap.config.modmap = [
    {
      name = "Remap CapsLock to Esc";
      remap = {"KEY_CAPSLOCK" = "KEY_ESC";};
    }
    {
      name = "Remap RightShift to Backspace";
      remap = {"KEY_RIGHTSHIFT" = "KEY_BACKSPACE";};
    }
  ];
}
