{
  inputs,
  vars,
  ...
}: {
  programs.hyprland = {
    enable = true;
    package = inputs.hyprland.packages.${vars.system}.hyprland;
    xwayland.enable = true;
  };

  environment.variables = {
    MOZ_ENABLE_WAYLAND = "1";
    MOZ_DISABLE_RDD_SANDBOX = "1";
    MOZ_WEBRENDER = "1";
    QT_QPA_PLATFORM = "wayland";
    QT_QPA_PLATFORMTHEME = "qt6ct";
    QT_WAYLAND_DISABLE_WINDOWDECORATION = "1";
    XCURSOR_THEME = "everforest-cursors-light";
    XCURSOR_SIZE = "32";
    "_JAVA_AWT_WM_NONREPARENTING" = "1";
    GDK_BACKEND = "wayland";
    NIXOS_OZONE_WL = "1";
    SDL_VIDEODRIVER = "wayland";
    WLR_DRM_NO_ATOMIC = "1";
  };
}
