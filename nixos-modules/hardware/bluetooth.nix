{
  hardware.bluetooth = {
    enable = true;
    powerOnBoot = false;
    disabledPlugins = [
      "csip"
      "micp"
      "vcp"
      "mcp"
      "bass"
      "bap"
    ];
  };

  services.blueman.enable = true;
}
