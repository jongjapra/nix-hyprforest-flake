# RESOURCE: https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/processor_mmio_stale_data.html
{
  pkgs,
  vars,
  ...
}: {
  hardware.enableAllFirmware = true;

  nixpkgs.hostPlatform = vars.system;

  boot = {
    kernelPackages = pkgs.linuxPackages_5_15;
    kernelParams = [
      "quiet"
      "splash"
      "nosgx"
      "mmio_stale_data=full,nosmt"
    ];
    initrd.availableKernelModules = [
      "xhci_pci"
      "ahci"
      "nvme"
      "usb_storage"
      "sd_mod"
    ];
    kernelModules = ["kvm-intel"];
  };
}
