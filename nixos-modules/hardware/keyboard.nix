{vars, ...}: {
  services.xserver.xkb = {
    layout = "us";
    variant = vars.keyboard;
  };

  console.useXkbConfig = true;

  services.logind.powerKey = "ignore";
}
