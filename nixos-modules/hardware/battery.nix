# RESOURCE: https://bbs.archlinux.org/viewtopic.php?id=260769
{
  boot.kernelParams = ["intel_pstate=active"];

  services = {
    power-profiles-daemon.enable = true;
    thermald.enable = true;
    acpid.enable = true;
  };

  powerManagement = {
    enable = true;
    cpuFreqGovernor = "powersave";
    powertop.enable = true;
  };
}
