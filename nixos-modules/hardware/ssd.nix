{
  boot.kernel.sysctl = {"vm.swappiness" = 0;};

  services.fstrim.enable = true;
}
