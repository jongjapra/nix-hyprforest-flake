{
  config,
  vars,
  ...
}: {
  # Realtek RTL8821CE
  boot = {
    extraModulePackages = [config.boot.kernelPackages.rtl8821ce];
    blacklistedKernelModules = ["rtw88_8821ce"];
  };

  networking = {
    hostName = vars.hostname;
    networkmanager = {
      enable = true;
      ethernet.macAddress = "random";
      wifi.macAddress = "random";
    };
  };

  programs.nm-applet.enable = true;
}
