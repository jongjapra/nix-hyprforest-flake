{
  services.xserver.libinput.enable = true;
  services.xserver.libinput.touchpad = {
    clickMethod = "clickfinger";
    disableWhileTyping = true;
    naturalScrolling = true;
  };
}
