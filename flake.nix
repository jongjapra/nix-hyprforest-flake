{
  description = "NixOS HyprForest Flake";

  nixConfig = {
    extra-substituters = [
      "https://nix-community.cachix.org"
      "https://hyprland.cachix.org"
    ];
    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "hyprland.cachix.org-1:a7pgxzMz7+chwVL3/pzj6jIBMioiJM7ypFP8PwtkuGc="
    ];
  };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    nix-colors.url = "github:misterio77/nix-colors";
    hyprland.url = "github:hyprwm/Hyprland";
    xremap.url = "github:xremap/nix-flake";
    disko.url = "github:nix-community/disko";
    disko.inputs.nixpkgs.follows = "nixpkgs";
    flakelight.url = "github:nix-community/flakelight";
    flakelight.inputs.nixpkgs.follows = "nixpkgs";
    home-manager.url = "github:nix-community/home-manager";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
    nix-index-database.url = "github:nix-community/nix-index-database";
    nix-index-database.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = {flakelight, ...} @ inputs:
    flakelight ./. {
      inherit inputs;
      nixDir = ./.;
      nixDirAliases = {
        nixosConfigurations = ["devices"];
        nixosModules = ["nixos-modules"];
        homeModules = ["home-modules"];
        packages = ["flake-packages"];
      };
      apps.default = pkgs: "${pkgs.install}/bin/install";
      devShell.packages = pkgs:
        with pkgs; [
          # Formatter
          alejandra
          nodePackages.prettier
          shfmt

          # Utility
          go-task
          neovim
          nh
        ];
      devShell.shellHook = ''
        echo -e "\033[1;36mWelcome to HyprForest Shell\033[0m"
        task --list
      '';
      flakelight.builtinFormatters = false;
      formatters = {
        "*.nix" = "alejandra";
        "*.sh" = "shfmt -w";
        "*.yml" = "prettier -w";
      };
    };
}
