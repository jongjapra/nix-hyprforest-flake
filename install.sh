#!/bin/sh

# Define color codes
RED='\033[0;31m'
YELLOW='\033[1;33m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color

# Confirm user is ready
read -rp "Are you ready to install NixOS from HyprForest Flake? Have you booted from the NixOS ISO installer? (y/n) " -r
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
	echo -e "${RED}Please boot from the NixOS ISO installer before proceeding.${NC}"
	exit 1
fi

# Clone HyprForest Flake
echo -e "${YELLOW}Cloning HyprForest Flake...${NC}"
nix-shell -p git --command "git clone https://gitlab.com/jongjapra/nix-hyprforest-flake.git /tmp/dotfiles"
cd /tmp/dotfiles || exit

# Get device variable from variables.nix
DEVICE=$(grep -oP '(?<=device = ").*(?=";)' variables.nix)

# Get user information
echo -e "${YELLOW}Please enter your details...${NC}"
read -rp "Enter hostname: " HOSTNAME
read -rp "Enter fullname: " FULLNAME
read -rp "Enter username: " USERNAME

while true; do
	read -rp "Enter email: " EMAIL
	if [[ "$EMAIL" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$ ]]; then
		echo "Email address confirmed."
		break
	else
		echo -e "${RED}Invalid email format. Please enter a valid email address.${NC}"
	fi
done

while true; do
	read -rsp "Enter password: " PASSWORD
	echo
	read -rsp "Confirm password: " CONFIRM_PASSWORD
	echo

	if [ "$PASSWORD" != "$CONFIRM_PASSWORD" ]; then
		echo -e "${RED}Passwords do not match. Please try again.${NC}"
	elif [ ${#PASSWORD} -lt 8 ]; then
		echo -e "${RED}Password must be at least 8 characters long. Please try again.${NC}"
	else
		echo "Password confirmed."
		PASSWORD_HASH=$(echo "$PASSWORD" | mkpasswd -m sha-512 -s)
		break
	fi
done

# Update variables.nix with user input
sed -i "/hostname =/c\  hostname = \"$HOSTNAME\";" variables.nix
sed -i "/fullname =/c\  fullname = \"$FULLNAME\";" variables.nix
sed -i "/username =/c\  username = \"$USERNAME\";" variables.nix
sed -i "/email =/c\  email = \"$EMAIL\";" variables.nix
sed -i "/password =/c\  password = \"$PASSWORD_HASH\";" variables.nix

# Format Disk
echo -e "${YELLOW}Formatting Disk...${NC}"
sudo nix --experimental-features "nix-command flakes" run github:nix-community/disko -- --mode disko ./devices/"$DEVICE"/disko-config.nix

# Install NixOS
echo -e "${YELLOW}Installing NixOS...${NC}"
sudo nixos-install --no-root-passwd --flake .#"$DEVICE"

# Copy HyprForest Flake
echo -e "${YELLOW}Copying HyprForest Flake to ~/Code...${NC}"
mkdir /mnt/home/"$USERNAME"/Code/
cp -r /tmp/dotfiles/ /mnt/home/"$USERNAME"/Code/

# Installation complete
echo -e "${GREEN}Installation is complete. Please reboot and remove the bootable disk.${NC}"
