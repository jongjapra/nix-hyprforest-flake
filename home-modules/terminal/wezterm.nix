# RESOURCE: https://git.sr.ht/~maksim/wezterm-everforest
{
  programs.wezterm = {
    enable = true;
    colorSchemes.everforest-dark-hard = {
      ansi = ["#4b565c" "#e67e80" "#a7c080" "#dbbc7f" "#7fbbb3" "#d699b6" "#83c092" "#d3c6aa"];
      brights = ["#4b565c" "#e67e80" "#a7c080" "#dbbc7f" "#7fbbb3" "#d699b6" "#83c092" "#d3c6aa"];
      foreground = "#d3c6aa";
      background = "#2b3339";
      cursor_bg = "#d3c6aa";
      cursor_border = "#d3c6aa";
      cursor_fg = "#2b3339";
      selection_bg = "#d3c6aa";
      selection_fg = "#2b3339";
    };
    extraConfig = ''
      return {
        font_size = 16.0,
        color_scheme = "everforest-dark-hard",

        disable_default_key_bindings = true,
        leader = { key=" ", mods="CTRL" },
        keys = {
          { key = "C", mods = "CTRL|SHIFT",   action=wezterm.action{CopyTo="ClipboardAndPrimarySelection"}},
          { key = "V", mods = "CTRL|SHIFT",   action=wezterm.action{PasteFrom="Clipboard"}},
          { key = "c", mods = "LEADER",       action=wezterm.action{SpawnTab="CurrentPaneDomain"}},
          { key = "q", mods = "LEADER",       action=wezterm.action{CloseCurrentPane={confirm=true}}},
          { key = "]", mods = "LEADER",       action=wezterm.action{ActivateTabRelative=1}},
          { key = "[", mods = "LEADER",       action=wezterm.action{ActivateTabRelative=-1}},
          { key = "1", mods = "LEADER",       action=wezterm.action{ActivateTab=0}},
          { key = "2", mods = "LEADER",       action=wezterm.action{ActivateTab=1}},
          { key = "3", mods = "LEADER",       action=wezterm.action{ActivateTab=2}},
          { key = "4", mods = "LEADER",       action=wezterm.action{ActivateTab=3}},
          { key = "5", mods = "LEADER",       action=wezterm.action{ActivateTab=4}},
          { key = "6", mods = "LEADER",       action=wezterm.action{ActivateTab=5}},
          { key = "7", mods = "LEADER",       action=wezterm.action{ActivateTab=6}},
          { key = "8", mods = "LEADER",       action=wezterm.action{ActivateTab=7}},
          { key = "9", mods = "LEADER",       action=wezterm.action{ActivateTab=8}},
          { key = "0", mods = "LEADER",       action=wezterm.action{ActivateTab=9}},
          { key = "-", mods = "LEADER",       action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}},
          { key = "\\",mods = "LEADER",       action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
          { key = "f", mods = "LEADER",       action="TogglePaneZoomState"},
          { key = "h", mods = "LEADER",       action=wezterm.action{ActivatePaneDirection="Left"}},
          { key = "j", mods = "LEADER",       action=wezterm.action{ActivatePaneDirection="Down"}},
          { key = "k", mods = "LEADER",       action=wezterm.action{ActivatePaneDirection="Up"}},
          { key = "l", mods = "LEADER",       action=wezterm.action{ActivatePaneDirection="Right"}},
          { key = "H", mods = "LEADER|SHIFT", action=wezterm.action{AdjustPaneSize={"Left",20}}},
          { key = "J", mods = "LEADER|SHIFT", action=wezterm.action{AdjustPaneSize={"Down",5}}},
          { key = "K", mods = "LEADER|SHIFT", action=wezterm.action{AdjustPaneSize={"Up",5}}},
          { key = "L", mods = "LEADER|SHIFT", action=wezterm.action{AdjustPaneSize={"Right",20}}},
        },
      }
    '';
  };
}
