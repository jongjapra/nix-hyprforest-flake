{pkgs, ...}: {
  programs.fish = {
    enable = true;
    functions = {
      fish_greeting = "";
    };
    plugins = with pkgs.fishPlugins; [
      {
        name = "autopair";
        src = autopair.src;
      }
      {
        name = "plugin-git";
        src = plugin-git.src;
      }
      {
        name = "puffer";
        src = puffer.src;
      }
      {
        name = "sponge";
        src = sponge.src;
      }
      {
        name = "z";
        src = z.src;
      }
    ];
  };
}
