{vars, ...}: {
  programs.git = with vars; {
    enable = true;
    userName = fullname;
    userEmail = email;
    extraConfig.init.defaultBranch = "main";
  };
}
