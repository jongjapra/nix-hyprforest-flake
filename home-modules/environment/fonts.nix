{pkgs, ...}: {
  fonts.fontconfig.enable = true;

  home.packages = with pkgs; [
    ubuntu_font_family
    font-awesome
    (nerdfonts.override {fonts = ["FiraCode"];})
  ];
}
