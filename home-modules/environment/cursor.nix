{pkgs, ...}: {
  home.pointerCursor = {
    gtk.enable = true;
    name = "everforest-cursors-light";
    package = pkgs.everforest-cursors;
    size = 32;
  };
}
