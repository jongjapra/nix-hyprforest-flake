{pkgs, ...}: {
  home.packages = with pkgs; [
    freetube
    obsidian
    vesktop
    whatsapp-for-linux
  ];
}
