{pkgs, ...}: {
  gtk = {
    enable = true;
    theme = {
      name = "Everforest-Dark-BL";
      package = pkgs.everforest-gtk;
    };
    iconTheme = {
      name = "Everforest-Dark";
      package = pkgs.everforest-icons;
    };
    font = {
      name = "Ubuntu";
      size = 12;
    };
  };
}
