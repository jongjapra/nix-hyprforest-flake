{pkgs, ...}: {
  programs.librewolf = {
    enable = true;
    package = pkgs.librewolf-wayland;
    settings = {
      # Disable exit with Ctrl+Q warning
      "browser.warnOnQuitShortcut" = false;
      # Enable 125% Zoom
      "toolkit.zoomManager.zoomValues" = ".3,.5,.67,.8,.9,1,1.1,1.2,1.25,1.33,1.5,1.7,2,2.4,3,4,5";
      # Enable Do Not Track on header
      "privacy.donottrackheader.enabled" = true;
      # Enable Firefox Sync
      "identity.fxaccounts.enabled" = true;
      # Bookmarks
      "browser.toolbars.bookmarks.visibility" = "never";
      "browser.tabs.loadBookmarksInTabs" = true;
      "browser.tabs.loadBookmarksInBackground" = true;
      "browser.bookmarks.openInTabClosesMenu" = false;
      # Hardware Acceleration
      "gfx.webrender.all" = true;
      "media.ffmpeg.vaapi.enabled" = true;
    };
  };

  home.file.".librewolf/librewolf.overrides.cfg".text = ''
    // DNS over HTTPS
    pref("network.trr.mode", 3);
    pref("network.trr.uri", "https://cloudflare-dns.com/dns-query");
  '';
}
