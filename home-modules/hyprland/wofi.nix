{config, ...}: let
  colors = config.colorScheme.palette;
in {
  programs.wofi = {
    enable = true;
    settings = {
      show = "drun";
      width = "25%";
      height = "38%";
      prompt = "Search....";
      normal_window = false;
      location = "center";
      gtk-dark = true;
      allow_images = true;
      image_size = 32;
      insensitive = true;
      allow_markup = true;
      no_actions = true;
      orientation = "vertical";
      halign = "fill";
      content_halign = "fill";
    };
    style = ''
      window {
          padding: 50px;
          border: 2px solid #${colors.base0D};
          border-radius: 15px;
          background-color: #${colors.base01};
          font-family: Ubuntu;
      	  font-size: 16px;
      }

      #input {
          margin: 20px 15px;
          padding: 10px 20px;
          background-color: #${colors.base02};
          border: none;
          color: #${colors.base05};
      }

      #input:focus {
          box-shadow: none;
      }

      #inner-box {
          margin: 0 15px;
      }

      #img {
          margin: 10px;
      }

      #entry:selected {
          background-color: #${colors.base0D};
          border-radius: 5px;
      }

      #text {
          color: #${colors.base05};
      }

      #text:selected {
          color: #${colors.base01};
      }
    '';
  };
}
