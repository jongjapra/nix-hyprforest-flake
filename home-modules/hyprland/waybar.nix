{config, ...}: let
  colors = config.colorScheme.palette;
in {
  programs.waybar = {
    enable = true;
    settings.mainBar = {
      layer = "top";
      position = "top";
      modules-center = [];
      modules-left = [
        "hyprland/workspaces"
        "cpu"
        "memory"
        "network"
      ];
      modules-right = [
        "tray"
        "pulseaudio"
        "battery"
        "clock"
      ];
      "hyprland/window" = {
        format = "{}";
        max-length = 50;
      };
      "hyprland/workspaces" = {
        format = "{icon}";
        format-icons = {
          default = "";
          active = "󰮯";
        };
        persistent-workspaces = {
          "*" = 5;
        };
        on-click = "activate";
      };
      cpu = {
        format = "   {usage}%";
        max-length = 10;
        interval = 5;
      };
      memory = {
        format = "   {used:0.1f}G/{total:0.1f}G";
        interval = 30;
      };
      tray = {
        icon-size = 15;
        tooltip = false;
        spacing = 10;
      };
      clock = {
        format = "{:󰥔   %R     󰃭   %A %d}";
        tooltip-format = "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>";
      };
      network = {
        format-wifi = "󰤨   {essid} {bandwidthTotalBytes}";
        format-disconnected = "󰤭 ";
        tooltip-format-wifi = "Signal Strength {signalStrength}%";
        tooltip-format-disconnected = "Disconnected";
        max-length = 50;
        interval = 5;
      };
      pulseaudio = {
        format = "{icon}  {volume}%";
        format-muted = "    Muted";
        format-icons = {
          default = ["" "" " "];
        };
        scroll-step = 5;
        on-click = "pulsemixer --toggle-mute";
        tooltip = false;
      };
      battery = {
        states = {
          warning = 20;
          critical = 10;
        };
        format = "{icon}  {capacity}%";
        format-icons = ["󰁺" "󰁻" "󰁼" "󰁽" "󰁾" "󰁿" "󰂀" "󰂁" "󰂂" "󰁹"];
        format-charging = "󰂄  {capacity}%";
        format-plugged = "󱘖   {capacity}%";
        on-click = "";
        interval = 10;
        tooltip = false;
      };
    };
    style = ''
      * {
          border: none;
          font-family: Ubuntu;
          font-size: 15px;
          min-height: 0;
      }

      window#waybar {
          background: #${colors.base00};
      }

      tooltip {
          background: #${colors.base00};
          border-color: #${colors.base0D};
          border-radius: 8px;
          border-width: 2px;
          border-style: solid;
      }

      #workspaces,
      #cpu,
      #memory,
      #network,
      #tray,
      #pulseaudio,
      #battery,
      #clock,
      #window {
          background: #${colors.base02};
          border-radius: 8px;
          border: 1px solid #${colors.base02};
          font-weight: 600;
          margin: 6px 0 6px 0;
          padding: 3.5px 16px;
      }

      #tray {
          margin-right: 10px;
          padding-left: 10px;
          padding-right: 10px;
      }

      #workspaces {
          font-weight: Bold;
          margin-left: 10px;
          margin-right: 10px;
          padding-left: 3.5px;
          padding-right: 3.5px;
      }

      #workspaces :nth-child(5) {
          margin-right: 0px;
      }

      #workspaces button {
          border-radius: 6px;
          color: #${colors.base05};
          padding: 6px;
          margin-right: 5px;
      }

      #workspaces button.active {
          background: #${colors.base0D};
          color: #${colors.base00};
      }

      #workspaces button:hover {
          background: #${colors.base00};
          color: #${colors.base0D};
      }

      #window {
          background: transparent;
          border-radius: 10px;
          margin-left: 60px;
          margin-right: 60px;
      }

      #cpu,
      #memory {
          color: #${colors.base0E};
      }

      #cpu {
          border-top-right-radius: 0;
          border-bottom-right-radius: 0;
          padding-right: 0;
      }

      #memory {
          border-top-left-radius: 0;
          border-bottom-left-radius: 0;
      }

      #clock {
          border-right: 0px;
          color: #${colors.base05};
          font-weight: 600;
          margin-right: 10px;
      }

      #network {
          color: #${colors.base08};
          margin-left: 10px;
      }

      #pulseaudio {
          border-left: 0px;
          border-right: 0px;
          color: #${colors.base0A};
          margin-right: 10px;
      }

      #battery {
          border-left: 0px;
          border-right: 0px;
          margin-right: 10px;
      }

      #battery.discharging {
          color: #${colors.base05};
      }

      #battery.charging,
      #battery.full {
          color: #${colors.base0D};
      }

      #battery.plugged {
          color: #${colors.base0E};
      }

      #battery.warning:not(.charging) {
          color: #${colors.base0C};
      }

      #battery.critical:not(.charging) {
          color: #${colors.base0E};
      }
    '';
  };
}
