{
  config,
  vars,
  ...
}: let
  colors = config.colorScheme.palette;
in {
  wayland.windowManager.hyprland = {
    enable = true;
    systemd.enable = true;
    settings = {
      monitor = "eDP-1,1920x1080@60,0x0,1";
      xwayland.force_zero_scaling = true;
      input = {
        kb_layout = "us";
        kb_variant = vars.keyboard;
        follow_mouse = 0; # Disabled
        sensitivity = 0;
        touchpad = {
          natural_scroll = true;
          clickfinger_behavior = 1;
          disable_while_typing = false;
          drag_lock = true;
        };
      };
      general = {
        gaps_in = 4;
        gaps_out = 8;
        border_size = 2;
        "col.active_border" = "rgb(${colors.base0D})";
        "col.inactive_border" = "rgb(${colors.base00})";
        layout = "dwindle";
        allow_tearing = true;
        # Whether to apply the sensitivity to raw input (e.g. used by games where you aim using your mouse)
        apply_sens_to_raw = true;
      };
      decoration = {
        rounding = 10;
        blur = {
          enabled = true;
          size = 3;
          passes = 1;
        };
        drop_shadow = true;
        shadow_range = 4;
        shadow_render_power = 3;
        "col.shadow" = "rgba(1a1a1aee)";
      };
      animations = {
        enabled = true;
        bezier = [
          "myBezier, 0.05, 0.9, 0.1, 1.05"
        ];
        animation = [
          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
        ];
      };
      dwindle.pseudotile = true;
      dwindle.preserve_split = true;
      master.new_is_master = true;
      gestures.workspace_swipe = false;
      misc = {
        vfr = true;
        vrr = 1;
        force_default_wallpaper = 0;
      };
      exec-once = [
        "waybar"
        "blueman-applet"
        "sleep 5 && vesktop --start-minimized"
        "sleep 5 && whatsapp-for-linux"
      ];
      "$mainMod" = "SUPER";
      bind = [
        "$mainMod, RETURN, exec, wezterm"
        "$mainMod, R, exec, pkill wofi || wofi"
        "$mainMod, W, exec, librewolf"
        "$mainMod SHIFT, W, exec, librewolf --private-window"

        "$mainMod, Q, killactive,"
        "$mainMod, F, fullscreen,"
        "$mainMod, SPACE, togglefloating,"
        "$mainMod, BACKSLASH, togglesplit,"

        "$mainMod, H, movefocus, l"
        "$mainMod, J, movefocus, d"
        "$mainMod, K, movefocus, u"
        "$mainMod, L, movefocus, r"

        "$mainMod SHIFT, H, swapwindow, l"
        "$mainMod SHIFT, J, swapwindow, d"
        "$mainMod SHIFT, K, swapwindow, u"
        "$mainMod SHIFT, L, swapwindow, r"

        "CTRL ALT, H, moveactive, -30 0"
        "CTRL ALT, J, moveactive, 0 30"
        "CTRL ALT, K, moveactive, 0 -30"
        "CTRL ALT, L, moveactive, 30 0"

        "CTRL SHIFT, H, resizeactive, -30 0"
        "CTRL SHIFT, J, resizeactive, 0 30"
        "CTRL SHIFT, K, resizeactive, 0 -30"
        "CTRL SHIFT, L, resizeactive, 30 0"

        "$mainMod, 1, workspace, 1"
        "$mainMod, 2, workspace, 2"
        "$mainMod, 3, workspace, 3"
        "$mainMod, 4, workspace, 4"
        "$mainMod, 5, workspace, 5"
        "$mainMod, 6, workspace, 6"
        "$mainMod, 7, workspace, 7"
        "$mainMod, 8, workspace, 8"
        "$mainMod, 9, workspace, 9"
        "$mainMod, 0, workspace, 10"

        "$mainMod SHIFT, 1, movetoworkspace, 1"
        "$mainMod SHIFT, 2, movetoworkspace, 2"
        "$mainMod SHIFT, 3, movetoworkspace, 3"
        "$mainMod SHIFT, 4, movetoworkspace, 4"
        "$mainMod SHIFT, 5, movetoworkspace, 5"
        "$mainMod SHIFT, 6, movetoworkspace, 6"
        "$mainMod SHIFT, 7, movetoworkspace, 7"
        "$mainMod SHIFT, 8, movetoworkspace, 8"
        "$mainMod SHIFT, 9, movetoworkspace, 9"
        "$mainMod SHIFT, 0, movetoworkspace, 10"

        "$mainMod, BRACKETRIGHT, workspace, e+1"
        "$mainMod, BRACKETLEFT, workspace, e-1"

        ", XF86MONBRIGHTNESSUP, exec, brightnessctl set 5%+"
        ", XF86MONBRIGHTNESSDOWN, exec, brightnessctl set 5%-"

        ", XF86AUDIOMUTE, exec, pulsemixer --toggle-mute"
        ", XF86AUDIORAISEVOLUME, exec, pulsemixer --change-volume +5 --max-volume 150"
        ", XF86AUDIOLOWERVOLUME, exec, pulsemixer --change-volume -5 --max-volume 150"
      ];
    };
  };
}
